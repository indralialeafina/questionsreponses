﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace Exemple_formulaire
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // Enonce de questions
            // Choix de reponses
            // Bonne reponse
            // Nombre de points?
            
            // Methode pour corriger la question, montrer la bonne reponse et donner des points?
            List<Question> questions = new List<Question>();
            //Questionnaire questionnaire = new Questionnaire(5);
            //Console.WriteLine(questionnaire.Questions.Length);
            
            // 2 facons de faire - see below
            /*Question questionAInserer = new Question("Lequel de ces mots est une saison?", 
                new []{"Patate","Poil","Maudit cave", "Été"},3,10);
            questionnaire1.ListeQuestions[0] = questionAInserer;*/
            
            questions.Add(new Question("Lequel de ces mots est une saison?", 
                new []{"Patate","Poil","Maudit cave", "Été"},3,10));
            
            questions.Add(new Question("Quel est le mot espagnol pour le mot : question ",
                new[] {"Beluga", "Amiga", "Pregunta", "Aloha"}, 2, 100));
            
            questions.Add(new Question("Quel est la capitale de l'Espagne?",
                new[] {"Madrid", "Barcelona", "Rome", "Prague"}, 0, 200));
            
            questions.Add( new Question("Quel est le mot anglais pour le mot : réponse ",
                new[] {"Comment", "Answer", "Respuesta", "Reponse"}, 1, 300));
            
            questions.Add( new Question("Vrai ou Faux: Est-ce que Shawinigan est en Estrie ?",
                new[] {"Vrai", "Faux"}, 1, 500));
            
            /*questionnaire.Questions[4] = new Question("Vrai ou Faux: Est-ce que Shawinigan est en Estrie ?",
                new[] {"Vrai", "Faux"}, 1, 500);
            */
            int score = 0;
            int scoreMax = 0;
            foreach (Question questionCourante in questions)
            {
                scoreMax += questionCourante.NombrePoints;
                score += PoserQuestion(questionCourante);
            }
            
            Console.WriteLine("Votre score est de " + score + " sur " + scoreMax + " donc " + Math.Round((double)score/scoreMax*100,2)+"%");
        }
        
        public static int PoserQuestion(Question question)
        {
            Console.WriteLine(question.EnonceQuestion);
            for (int i = 0; i < question.ChoixReponse.Length; i++)
            {
                Console.WriteLine(i+1 + " " + question.ChoixReponse[i]);
            }

            int choix = int.Parse(Console.ReadLine());
            if (choix - 1 == question.BonneReponse)
            {
                Console.WriteLine("Bonne réponse !!");
                return question.NombrePoints;
            }
            Console.WriteLine("Mauvaise réponse !!");
            return 0;
        }
    }
    
}