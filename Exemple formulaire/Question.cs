using System.Dynamic;

namespace Exemple_formulaire
{
    public class Question
    {
        public string EnonceQuestion { get; set; }
        public string [] ChoixReponse { get; set; }
        public int BonneReponse { get; set; }
        public int NombrePoints { get; set; }

        public Question(string enonceQuestion, string[] choixReponse, int bonneReponse, int nombrePoints)
        {
            EnonceQuestion = enonceQuestion;
            ChoixReponse = choixReponse;
            BonneReponse = bonneReponse;
            NombrePoints = nombrePoints;
        }
    }
}